function getInfo() {
  const validBusses = ["1287", "1308", "1327", "2334"];
  const stopId = document.getElementById("stopId");
  const stopInfo = document.getElementById("stopName");
  const busesUList = document.getElementById("buses");

  if (!validBusses.includes(stopId.value)) {
    stopInfo.textContent = "Error";
    return;
  }

  const url = `https://judgetests.firebaseio.com/businfo/${stopId.value}.json`;

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      busesUList.innerHTML = '';
      stopInfo.textContent = data.name;
      
      Object.keys(data.buses).forEach(k => {
        const li = document.createElement('li');

        li.textContent = `Bus ${k} arrives in ${data.buses[k]} minutes`;
        busesUList.appendChild(li);
      });
    });

    stopId.value = '';
}
