function solve() {
  const baseUrl = `https://judgetests.firebaseio.com/schedule/`;
  let stopId = "depot";
  const info = document.getElementById("info");
  let stopName;

  function changeBtn() {
    const departDis = document.getElementById("depart");
    const arriveDis = document.getElementById("arrive");
    if (departDis.disabled) {
      departDis.disabled = false;
      arriveDis.disabled = true;
    } else {
      arriveDis.disabled = false;
      departDis.disabled = true;
    }
  }

  function depart() {
    const url = `${baseUrl}${stopId}.json`;

    fetch(url)
      .then((r) => r.json())
      .then((data) => {
        info.textContent = `Next stop ${data.name}`;
        stopId = data.next;
        stopName = data.name;
      });

    changeBtn();
  }

  function arrive() {
    info.textContent = `Arriving at ${stopName}`;
    changeBtn();
  }

  return {
    depart,
    arrive,
  };
}

let result = solve();
