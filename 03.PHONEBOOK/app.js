function attachEvents() {
  const baseUrl = `https://phonebook-nakov.firebaseio.com/phonebook.json`;

  const createBtn = document.getElementById("btnCreate");
  const loadBtn = document.getElementById("btnLoad");
  const phoneBookUList = document.getElementById("phonebook");

  function load() {
    phoneBookUList.innerHTML = "";

    fetch(baseUrl)
      .then((r) => r.json())
      .then((d) => {
        Object.keys(d).forEach((k) => {
          const li = document.createElement("li");

          li.textContent = `${d[k].person}: ${d[k].phone}`;

          let deleteBtn = document.createElement("button");
          deleteBtn.textContent = "DELETE";
          li.appendChild(deleteBtn);

          const deleteUrl = `https://phonebook-nakov.firebaseio.com/phonebook/${k}.json`;

          deleteBtn.addEventListener("click", () => {
            fetch(deleteUrl, {
              method: "DELETE",
            });
          });

          phoneBookUList.appendChild(li);
        });
      });
  }

  function create() {
    const person = document.getElementById("person").value;
    const phone = document.getElementById("phone").value;

    const personObj = { person, phone };

    fetch(`${baseUrl}`, {
      method: "POST",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify(personObj),
    });
  }

  loadBtn.addEventListener("click", load);
  createBtn.addEventListener("click", create);
}

attachEvents();
