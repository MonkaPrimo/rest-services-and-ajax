function attachEvents() {
    // Firebase URL
    const url = 'https://rest-messanger.firebaseio.com/messanger.json';

    // Buttons
    const sendBtn = document.getElementById('submit');
    const refreshBtn = document.getElementById('refresh');

    // Value Areas
    const textArea = document.getElementById('messages');
    const authorArea = document.getElementById('author');
    const contentArea = document.getElementById('content');

    function refreshMessenger() {
        fetch(url)
        .then(r => r.json())
        .then(d => {
            Object.keys(d).forEach(k => {
                const message = `${d[k].author}: ${d[k].content}`
                textArea.value += `${message}\r\n`;
            });
           textArea.scrollTop = textArea.scrollHeight;
        })
    }


    function addMessage() {
        const author = authorArea.value;
        const content = contentArea.value;
        
        const obj = {author, content}

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(obj)
        })

        refreshMessenger();
    }

    refreshBtn.addEventListener("click", refreshMessenger);
    sendBtn.addEventListener("click", addMessage);
}

attachEvents();